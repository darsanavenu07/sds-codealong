---
title: Getting Started with Python
subtitle: Installing Python
date: 2020-03-17
tags: ["python", "vscode", "interpreter"]
---
# Contents

1.  [Installing Python 3 interpreter on linux and windows](#org11e18f2)
2.  [Installing a text editor](#orgb0c6b0f)
3.  [Running Code in VS Code](#org359fcea)
4.  [Referrals](#org1b5465a)
5.  [Basic programs](#org42hjbn)


**Reference Resources**

-  http://wiki.python.org/moin/


<a id="org11e18f2"></a>

# Installing Python 3 interpreter

Interpreters are program that reads Python programs and carries out their instructions; you need it before you can do any Python programming. Mac and Linux distributions may include an outdated version of Python (Python 2), but you should install an updated one (Python 3).

- Windows Users
    Download [here](https://python.org/downloads/windows/)



If you're on a Linux system open the terminal and run one of the commands below based on your OS:

-   Ubuntu/Debian

    `sudo apt-get install python3`

-   Arch Linux

    `sudo pacman -Sy python3`

-   Fedora

    `sudo yum install python3`


<a id="orgb0c6b0f"></a>

# Installing a text editor

This is often a point of controversy, my advice here is to use whatever you're comfortable with or whatever is being used in your classes at your school or college. That being said I would personally suggest using [Visual Studio Code](https://code.visualstudio.com/) which in my opinion is a much better text editor and can be used as an IDE for multiple languages.

To install vs code follow the steps show in this [video](https://www.youtube.com/watch?v=8tkuu0Rugg4) if you're on windows.

On follow on of the following steps based on your distribution:

-   Ubuntu
    Follow the instructions in this [video](https://www.youtube.com/watch?v=mfxP0REDWs4)
-   Arch Linux
    Execute in terminal:
`sudo pacman -S code`
-   Fedora
    Follow the steps from this [link](https://tecadmin.net/install-visual-studio-code-editor-in-fedora/) or search your app-store.


<a id="org359fcea"></a>

# Running Code in VS Code

Reffer this [youtube](https://www.youtube.com/watch?v=dNFgRUD2w68) video for learning how to set up python development environment on visual studio code.

### For Linux users
Use any text editor of choice and save it in 'filename.py' format and to execute the program open terminal using the cd command locate to the folder to which the python file is saved, run the command 'python3 filename.py' in the terminal.


<a id="org1b5465a"></a>

# Referrals:

-   [Official Python Documentation](https://docs.python.org/3/)
-   [Practice resource](http://www.practicepython.org/)
-    Video tutorials:
-    [Udemy](https://www.udemy.com/python-for-absolute-beginners-u/)
    [Udacity](https://www.udacity.com/course/programming-foundations-with-python--ud036)

    [Cousera](https://www.coursera.org/learn/python3)

-   [Python Wiki](http://wiki.python.org/)

<a id="#org42hjbn"></a>
