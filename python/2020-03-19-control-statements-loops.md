**Conditional statements:**

Conditional statements perform different computations or actions depending on Boolean constraint.Python uses Boolean variables to evaluate conditions. The Boolean values True and False are returned when an expression is compared or evaluated. For example:
```python
x = 2
print(x == 2) //prints True
print(x == 3) // prints False
print(x < 3) //prints True
```



*  **If statement**

If Statement is used for decision making. It will run the body of code only when if statement is true.


**Syntax:**
```python
if condition:
 	Statement
else:
	statement
```
**Example:**
```python
a = 33
b = 200
 if b > a:
 print("b is greater than a")
```
*  **Else statement:**

	The "else condition" is usually used when you have to judge one statement on the basis of other. If one condition goes wrong, then there should be another condition that should justify the statement or logic. 

**Syntax:**
```python
 If (condition):
	Statement
else:
	Statement
```
**Example:**

```python
a = 200 
b = 33
 if b > a: 
print("b is greater than a") 
else: 
print("a is greater than b")
```

*  **Elif statement:**

The elif keyword is Python's way of saying "if the previous conditions were not true, then try this condition".

**Syntax:**

```python
If (condition):
	Statement
elif:(condition):
	Statement
else:
	Statement
```
**Example:**
```python
a = 200 
b = 33
 if b > a: 
print("b is greater than a") 
elif a == b: 	
print("a and b are equal") 
else: 
print("a is greater than b")
```


**LOOPS IN PYTHON**
*  **for loop**

A for loop is used for iterating over a sequence (it can be a list,a tuple or a string).

**Syntax:**

```python
for variable in sequence:
    statements(s)
```
**Example**
```python 
numbers=[1,2,3,4,5]
for i in numbers:
    print(i)
```
**Output**
```python
1
2
3
4 
5      
```

*  **while loop**

With while loop we can execute a set of statements as long as condition is true.

**Syntax:**

```python
while expression:
    statement(s)
```
**Example**
```python
i=1
while i < 5:
   print(i)
   i += 1
```
**Output**

```python
1
2
3
4        
```

**LOOP CONTROL STATEMENTS**

Control statements are used to alter or control the flow of loop execution based on the specified conditions. Python has the following loop control statements:
*  Break Statement
*  Continue Statement
*  Pass Statement

*   **break statement**

   It is used to break or terminate the execution of the current loop and control is transferred to the next statement following the loop.Basically brings out of the loop.

**Syntax**
 use break keyword.

```python 
for i in range(10):
  if (i==5):
    break
  else:
    print(i)
```
**Output**

```python
0
1
2
3
4        //terminates the loop!
```

*   **continue statement**

   It returns the control to the beginning of the loop. It skips the remaining statements and moves back the control to the top of the loop.

**Syntax**
 use continue keyword.

```python 
for i in range(10):
  if (i==5):
    continue
  else:
    print(i)
```
**Output**

```python
0
1
2
3
4
6             //skips 5 here!
7
8
9

```
 *  **pass statement**

   It is a null statement. Nothing happens when it is executed. Can be used to write empty loops.

**Syntax**
 use pass keyword.

```python 
for i in range(10):
   pass
print(i)
```
**Output**

```python
9
```
```